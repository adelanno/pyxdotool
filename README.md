# `python` wrapper for [`xdotool`](https://github.com/jordansissel/xdotool)

intended to be used from the CMS control room machines to programmatically launch any graphical window of specific size & position

note that `xdotool` is not currently installed in the CMS control room machines. thus, the source code is first cloned and compiled from lxplus and then transferred to the intended host in the cmsusr cluster:

```bash
ssh lxplus
cms_host='brilshift@scx5scr69'
git clone git@github.com:jordansissel/xdotool.git ~/xdotool/
cd ~/xdotool
make && scp -r -o ProxyJump=${USER}@cmsusr ~/xdotool ${cms_host}:~/xdotool
```
