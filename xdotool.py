#!/usr/bin/env python3

import logging
import os
import pathlib
import subprocess
import time

logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')

SLEEP = 1.0

XDOTOOL_PATH = pathlib.Path.home()/'xdotool'
XDOTOOL = XDOTOOL_PATH/'xdotool'

ENV = os.environ.copy()
LD_LIBRARY_PATH = [path for path in ENV['LD_LIBRARY_PATH'].split(':') if path]
ENV['LD_LIBRARY_PATH'] = str.join(':', LD_LIBRARY_PATH + [str(XDOTOOL_PATH)])

def run(cmd: list[str, int, pathlib.Path], env: dict[str, str] = ENV, capture_output: bool = True, text: bool = True):
    return subprocess.run(args=list(map(str, cmd)), env=ENV, capture_output=capture_output, text=text)

def get_window_ids() -> list[int]:
    window_ids = run(cmd=['xprop', '-root', '_NET_CLIENT_LIST_STACKING']).stdout
    return [int(id.strip(), 16) for id in window_ids.split('#')[1].split(',')]

def get_window_info(window_id: int) -> dict[str, str]:
    CLASS = run(cmd=[XDOTOOL, 'getwindowclassname', window_id]).stdout.strip()
    NAME = run(cmd=[XDOTOOL, 'getwindowname', window_id]).stdout.strip()
    PID = run(cmd=[XDOTOOL, 'getwindowpid', window_id]).stdout.strip()
    GEOMETRY = run(cmd=[XDOTOOL, 'getwindowgeometry', '--shell', window_id]).stdout
    GEOMETRY = dict(p.split('=') for p in GEOMETRY.splitlines())
    return dict(CLASS=CLASS, NAME=NAME, PID=PID, **GEOMETRY)

def get_info_all_windows():
    for id in get_window_ids():
        logging.debug(get_window_info(id))

def get_active_window():
    return run(cmd=[XDOTOOL, 'getactivewindow']).stdout.strip()

def set_window_size(window_id: int, width: int, height: int):
    run(cmd=[XDOTOOL, 'windowsize', window_id, width, height])

def set_window_position(window_id: int, x: int, y: int):
    run(cmd=[XDOTOOL, 'windowmove', window_id, x, y])

def launch_process(args: list[str]) -> int:
    process = subprocess.Popen(args)
    time.sleep(SLEEP)
    window_id = get_active_window()
    return window_id

def launch_window(args: list[str], width: int, height: int, x: int, y: int):
    window_id = launch_process(args)
    logging.debug(get_window_info(window_id=window_id))
    logging.debug(f'({width}x{height}) @ ({x}, {y})')
    set_window_size(window_id=window_id, width=width, height=height)
    set_window_position(window_id=window_id, x=x, y=y)


class RightMost:

    def top_left():
        launch_window(args=['firefox', '-new-window', 'http://op-webtools.web.cern.ch/vistar?usr=LHC1'], width=1364, height=1156, x=5, y=29)
        # args = '/opt/WinCC_OA/3.19/bin/WCCOAui -PROJ ServiceProject -pmonIndex 1 -m vision:BRIL,1440x1200+2560+0 -data DCS-C2F43-24-04 DCS-S2F16-24-04 -event DCS-C2F43-24-04 DCS-S2F16-24-04 -p CMSfw_Console/CMSfw_ViewerCR.pnl -style windows'.split()
        # launch_window(args=args, width=1686, height=1295, x=1381, y=29)

    def top_right():
        launch_window(args=['firefox', '-new-window', 'http://srv-s2d16-22-01/webmonitor/background'], width=3830, height=750, x=3840, y=0)
        launch_window(args=['firefox', '-new-window', 'http://srv-s2d16-22-01/webmonitor/summary'], width=3830, height=1350, x=3840, y=750)

    def bottom_left():
        launch_window(args=['firefox', '-new-window', 'http://srv-s2d16-22-01/runcontrol/overview'], width=1660, height=2096, x=0, y=2190)
        launch_window(args=['firefox', '-new-window', 'http://bptxscope.cms/_index.html'], width=2160, height=1426, x=1670, y=2190)
        launch_window(args=['xfce4-terminal'], width=2160, height=667, x=1675, y=3648)

    def bottom_right():
        launch_window(args=['firefox', '-new-window', 'http://srv-s2d16-22-01/webmonitor/lumi'], width=3830, height=2136, x=3840, y=2160)

def main():
    logging.debug(subprocess.run(f'{XDOTOOL_PATH}/xdotool version'.split(), env=ENV, capture_output=True, text=True).stdout)
    RightMost.top_left()
    RightMost.top_right()
    RightMost.bottom_left()
    RightMost.bottom_right()

if __name__ == '__main__':
    main()


class Trash:

    def get_window_id_from_pid(pid: int) -> list[dict[str, str]]:
        windows = [get_window_info(id) for id in get_window_ids()]
        if not windows:
            return
        window = [window for window in windows if window['PID'] and int(window['PID']) == pid]
        if len(window) != 1:
            return
        logging.debug(window)
        return window_id[0]

    def search_window_name(name: str) -> list[int]:
        return list(map(int, run(cmd=[XDOTOOL, 'search', '--onlyvisible', '--name', name]).stdout.splitlines()))

    def search_window_pid(pid: int) -> list[int]:
        return list(map(int, run(cmd=[XDOTOOL, 'search', '--onlyvisible', '--pid', pid]).stdout.splitlines()))
